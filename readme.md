## About the app
- framework used: [Laravel](https://laravel.com/)
- all information needed for making the app run are added in .env file by copying .env.example to .env
- things not considered:
    - translations and config files
    - potential for multiple currencies
    - not adding the database schema to git ( adding it here for ease of use, usually it would go into docs)
- routing is in __routes/web.php__
- gates are defined in __\App\Providers\AuthServiceProvider__
- html is in __resources__
