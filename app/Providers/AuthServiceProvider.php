<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('report-index', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('report-show', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('transaction-index', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('user-index', function ($user) {
            return $user->isAdmin();
        });
    }
}
