<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 *
 * @package App\Models
 */
class Transaction extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'failed'];

    /**
     * Return a builder or a collection with the amount of the transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function amount()
    {
        return $this->hasOne(Amount::class);
    }

    /**
     * Return a builder or a collection with the user that owns the transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
