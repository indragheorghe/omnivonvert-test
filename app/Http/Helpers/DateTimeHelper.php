<?php


namespace App\Http\Helpers;


use Carbon\Carbon;
use Prophecy\Exception\Doubler\MethodNotFoundException;

/**
 * Class DateTimeHelper
 *
 * @package App\Http\Helpers
 */
class DateTimeHelper
{
    /* TODO: find a nicer way to implement*/

    /**
     * Generated the dates for the received period or throws an exception
     *
     * @param $period
     * @return mixed
     */
    public function generateDates($period)
    {
        if (method_exists($this, $period)) {
            return $this->$period();
        }
        throw new MethodNotFoundException("Method $period not found");
    }

    /**
     * Returns the dates for last week
     *
     * @return array
     */
    public function lastWeek(): array
    {
        return [Carbon::today()->subWeek()->startOfWeek(), Carbon::today()->subWeek()->endOfWeek()];
    }

    /**
     * Returns the dates for last month
     *
     * @return array
     */
    public function lastMonth(): array
    {
        return [Carbon::today()->subMonth()->startOfMonth(), Carbon::today()->subMonth()->endOfMonth()];
    }

    /**
     * Returns null because we don't care about dates when we calculate an al time report
     *
     * @return array
     */
    public function allTime(): array
    {
        return [null, null];
    }
}
