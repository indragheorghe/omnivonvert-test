<?php


namespace App\Http\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsHelper
 *
 * @package App\Http\Helpers
 */
class ReportsHelper
{

    /**
     * Top 5 users, by transactions - displayed as a list, with user ID and transaction count
     *
     * @param \Carbon\Carbon|null $start
     * @param \Carbon\Carbon|null $end
     * @param int $noOfUsers
     * @return \Illuminate\Support\Collection
     */
    public function getTopUsersByTransactionCount($start, $end, int $noOfUsers = 5): Collection
    {
        $list = DB::table('transactions')
            ->join('users', 'users.id', '=', 'transactions.user_id')
            ->select('users.id as userId', DB::RAW("COUNT('transactions.id') as transactionCount"))
            ->where('transactions.failed', '=', 0);
        if ($end !== null && $start !== null) {
            $list->whereBetween('transactions.created_at', [$start, $end]);
        }
        $list->groupBy('users.id')
            ->orderBy('transactionCount', 'desc')
            ->limit($noOfUsers);

        /*Actual SQL QUERY
            select `users`.`id`, COUNT('transactions.id') as transactionCount from `tranactions`
            inner join `users` on `users`.`id` = `transactions`.`user_id`
            where `transactions`.`failed` = 0 and `transactions`.`created_at`
            between '2019-09-28 11:25:28' and '2019-09-29 23:59:59' // optional
            group by `users`.`id`
            order by `transactionCount` desc
            limit 5
        */

        return $list->get();
    }


    /**
     * Total amount evolution, per day (for all users summed up) - displayed in a table or in a graph
     *
     * @param \Carbon\Carbon|null $start
     * @param \Carbon\Carbon|null $end
     * @return \Illuminate\Support\Collection
     */
    public function getAmountPerDay($start, $end): Collection
    {
        $list = DB::table('amounts')
            ->select(DB::RAW("DATE_FORMAT(created_at , '%Y-%m-%d') as date,ROUND(SUM(amount), 2) as dailyTotalAmount"));

        if ($end !== null && $start !== null) {
            $list->whereBetween('created_at', [$start, $end]);
        }
        $list->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"));

        /*Actual SQL QUERY
            select DATE_FORMAT(created_at , '%Y-%m-%d') as date,ROUND(SUM(amount), 2) as dailyTotalAmount
            from `amounts`
            where `created_at` between '2019-09-28 11:24:24' and '2019-09-29 23:59:59' // optional
            group by DATE_FORMAT(created_at, '%Y-%m-%d')
       */

        return $list->get();
    }
}
