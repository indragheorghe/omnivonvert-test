<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

/**
 * Class TransactionController
 *
 * @package App\Http\Controllers
 */
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('transaction-index')) {
            $transactions = Transaction::all();
            return view('admin.transaction')->withTransactions($transactions);
        }
        return redirect(route('transaction.show', auth()->user()->id));
    }

    /**
     * @param int $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $userId)
    {
        $user = User::findOrfail($userId);

        if (auth()->user()->hasPendingTransaction() && auth()->user()->id == $userId) {
            return view('amount');
        }

        $transactions = $user->transactions;

        return view('transaction')->withTransactions($transactions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->_token) {
            if (Transaction::create(['user_id' => auth()->user()->id])) {
                return view('amount');
            }
        }
        return view('transaction');

    }
}
