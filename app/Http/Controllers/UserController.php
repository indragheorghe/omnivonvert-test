<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Gate;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{

    /**
     * Display a listing of all the users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (!Gate::allows('user-index')) {
            abort(403);
        }
        $users = User::whereHas('role', function ($query){
            $query->where('name', 'user');
        })->get();
        return view('admin.users')->withUsers($users);
    }
}
