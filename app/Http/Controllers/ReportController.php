<?php

namespace App\Http\Controllers;

use App\Http\Helpers\DateTimeHelper;
use App\Http\Helpers\ReportsHelper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;

/**
 * Class ReportController
 *
 * @package App\Http\Controllers
 */
class ReportController extends Controller
{
    /**
     * Show the report page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (!Gate::allows('report-index')) {
            abort(403);
        }
        return view('reports');

    }

    /**
     * Return the report data
     *
     * @param $type
     * @param $period
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($type, $period, DateTimeHelper $helper)
    {
        if (!Gate::allows('report-index')) {
            abort(403);
        }
        /* TODO: change to strategy pattern */
        $reportHelper = new ReportsHelper();
        if ($type == 'top5') {
            $report = $reportHelper->getTopUsersByTransactionCount(...$helper->generateDates($period));
        } else {
            $report = $reportHelper->getAmountPerDay(...$helper->generateDates($period));
        }

        return Response::json($report);
    }
}
