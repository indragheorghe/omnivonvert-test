<?php

namespace App\Http\Controllers;

use App\Models\Amount;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Mockery\Exception\NoMatchingExpectationException;

class AmountController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = Transaction::findOrfail($request->transaction_id);

        if ($transaction->user_id != $request->user_id) {
            throw new NoMatchingExpectationException('User does not match transaction user');
        }
        $amount = Amount::create($request->except('user_id'));

        $amount->transaction->update(['failed' => 0]);

        return redirect('/home');
    }
}
