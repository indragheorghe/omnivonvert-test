<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class AmountStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'numeric|required',
            'amount' => 'numeric|required',
            'transaction' => 'numeric|required',
        ];
    }
}
