@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(auth()->user()->id == $user->id)
                    <div class="card">
                        <div class="card-header">{{ __('Transaction') }}</div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('transaction.store') }}">
                                @csrf

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Begin transaction') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Transaction list') }}</div>
                    <div class="card-body">
                        <ul>
                            @foreach($transactions as $transaction)
                                <p> Transaction from {{$transaction->created_at}}</p>
                                <li>
                                    Status: {{$transaction->failed == 0 ? 'paid' : ($transaction->failed == null ? 'pending' : 'failed') }}</li>
                                <li> User: {{$transaction->user->name}}</li>
                                <li> Amount: {{$transaction->amount->amount ?? 'n/a'}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
