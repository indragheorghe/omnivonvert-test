@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Transaction') }}</div>

                    <ul>
                        @foreach($transactions as $transaction)
                            <p> Transaction from {{$transaction->created_at}}</p>
                            <li>
                                Status: {{$transaction->failed == 0 ? 'paid' : ($transaction->failed == null ? 'pending' : 'failed') }}</li>
                            <li> User: <a href="{{route('transaction.show', $transaction->user->id)}}">
                                    {{$transaction->user->name}}
                                </a>
                            </li>
                            <li> Amount: {{$transaction->amount->amount ?? 'n/a'}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
