@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('User list') }}</div>

                    <ul>
                        @foreach($users as $user)
                            <li> <a href="{{route('transaction.show', $user->id)}}">
                                    {{$user->name}} | {{$user->email}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
