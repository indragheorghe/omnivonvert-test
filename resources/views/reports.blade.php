@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="form-group">
                        <label for="sel1">Report type</label>
                        <select class="form-control" id="report">
                            <option value="amount">Amount</option>
                            <option value="top5">Top 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Period</label>
                        <select class="form-control" id="period">
                            <option value="lastWeek">Last week</option>
                            <option value="lastMonth">Last month</option>
                            <option value="allTime">All time</option>
                        </select>
                    </div>
                    <button class="btn btn-primary" type="button" name="Show" onclick="load()">Load report</button>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <ul id="report-area">

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
  function load() {
    var report = document.getElementById("report");
    var reportType = report.options[report.selectedIndex].value;
    var period = document.getElementById("period");
    var reportPeriod = period.options[period.selectedIndex].value;

    getReport(reportType, reportPeriod);
  }

  function getReport(reportType, reportPeriod) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("report-area").innerHTML = "";
        let response = JSON.parse(this.responseText);
        response.forEach(addReportData);
      }
    };
    xhttp.open("GET", "/report/" + reportType + '/' + reportPeriod, true);
    xhttp.send();
  }

  function addReportData(item) {
    let html = "<li>" + JSON.stringify(item) + "</li>"
    document.getElementById("report-area").innerHTML += html;
  }
</script>
