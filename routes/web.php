<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('report/{type}/{period}', 'ReportController@show')->name('report.show');
    Route::get('report', 'ReportController@index')->name('report.index');

    Route::get('save', 'AmountController@store')->name('save');
    Route::get('users', 'UserController@index')->name('user.index');

    Route::resource('transaction', 'TransactionController');

    Route::get('home', 'TransactionController@index')->name('home');
});
